echo "****************************************************"
echo "Copying site files to local site folder ..."
echo "****************************************************"
echo ""

cp -f settings.php /var/www/html/web/sites/default
cp -f settings.local.php /var/www/html/web/sites/default
cp -f development.services.yml /var/www/html/web/sites/default
cp -f uw_news_site.sql /var/www/html/web/sites/default

echo ""
echo "****************************************************"
echo "Done copying site files."
echo "****************************************************"
echo ""

echo ""
echo "****************************************************"
echo "Building profile ..."
echo "****************************************************"
echo ""

cd /var/www/html/web/profiles

DIR="uw_news_profile"
if [ -d "$DIR" ]; then
  rm -rf "$DIR"
fi

git clone https://git.uwaterloo.ca/ece-651-drupal-project/uw_news_profile

cd uw_news_profile

composer install

echo ""
echo "****************************************************"
echo "Done building profile."
echo "****************************************************"
echo ""

echo ""
echo "****************************************************"
echo "Setting up local site folder ..."
echo "****************************************************"
echo ""

cd /var/www/html/web/sites/default

DIR="files"
if [ ! -d "$DIR" ]; then
  mkdir files
fi

sudo chmod 777 files

cd files

DIR="sync"
if [ -d "$DIR" ]; then
 rm -rf sync
fi

git clone https://git.uwaterloo.ca/ece-651-drupal-project/uw_news_config sync

sudo chmod 777 sync

echo ""
echo "****************************************************"
echo "Done setting up local site folder."
echo "****************************************************"
echo ""

cd ..

echo ""
echo "****************************************************"
echo "Importing site database ..."
echo "****************************************************"
echo ""

drush sql-drop -y
drush sql-cli < uw_news_site.sql

rm uw_news_site.sql

echo ""
echo "****************************************************"
echo "Done import site database"
echo "****************************************************"
echo ""

echo ""
echo "**************************************************************"
echo "All actions have been completed."
echo "Site is now installed at https://drupal.docker.localhost:8080"
echo "**************************************************************"
